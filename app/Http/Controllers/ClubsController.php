<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Club;
use App\Models\Jugador;

class ClubsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('clubs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        try {

            $club = new Club;
            $club->name = $request->name;
            $club->save();

            return back()->with('success', 'Se ha creado el club correctamente')->withInput();

        } catch (\Exception $e) {

            return back()->with('error', 'Ha ocurrido un error al guardarse el formulario')->withInput();

        }

    }

    /**
     * Display the specified resource.clea
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        try {
            
            $jugadores = Jugador::where('club_id', '=', $id)->delete();                        

            $club = Club::find($id);
            $club->delete();

            return back()->with('sucess', 'Se ha eliminado el club correctamente')->withInput();

        } catch(\Exception $e) {

            return back()->with('error', 'Ha ocurrido un error al eliminar el club')->withInput();

        }

    }
}
