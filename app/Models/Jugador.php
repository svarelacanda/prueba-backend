<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    use HasFactory;

    protected $table = 'jugadores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'habilitado',
        'club_id'
    ];


    /**
     * Devuelve el club de un jugador
     */
    public function club() {
        return $this->belongsTo(Club::class);
    }



}
