<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo jugador</title>
</head>
<body>
<div style="width=50%;height:300px;margin:auto;">
    <h2>Nuevo jugador</h2>
    <form action="{{ route('jugadores.store') }}" method="post">
        @csrf
        <input type="text" placeholder="Nombre del jugador" name="name">
        <select name="club_id">
            @foreach($clubs as $club)
                <option value="{{ $club->id }}">{{ $club->name }}</option>
            @endforeach
        </select>
        <button type="submit">Enviar</button>
    </form>
    </div>
</body>
</html>