<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Club</title>
</head>
<body>
    <div style="width=50%;height:300px;margin:auto;">
    <h2>Nuevo club</h2>
    <form action="{{ route('clubs.store') }}" method="post">
        @csrf
        <input type="text" placeholder="Nombre del club" name="name">
        <button type="submit">Enviar</button>
    </form>
    </div>
</body>
</html>